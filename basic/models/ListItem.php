<?php
namespace app\models;

use Yii;

class ListItem extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'listitem';
    }

    public function rules()
    {
        return [
            [['title', 'description', 'price', 'item_type', 'list_id'], 'required'],
        ];
    }

}

<?php
namespace app\models;

use Yii;

class ShoppingList extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'shoppinglist';
    }

    public function rules()
    {
        return [
            [['title', 'description'], 'required'],

        ];
    }

}

<?php
namespace app\models;

use Yii;

class Types extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'types';
    }

    public function rules()
    {
        return [
            [['type'], 'required'],
        ];
    }

}

<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<br/>
<?= Html::a('Add grocery list', ['site/create'], ['class' => 'button createButton'], ['data-toggle' => 'modal'], ['data-target' => '#myModal']); ?>

 <!--<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Open Modal</button>-->

<div class="grocery-list-container">
  <?php foreach($list as $field){ ?>
        <div>
          <?= $field->title; ?>
          <br/>
          <?= $field->list_date; ?>
          <br/>
          <?= $field->description; ?>
          <br/>
          <?= Html::a("View", ['site/view', 'id' => $field->id], ['class' => 'button customButton']); ?>
        </div>

  <?php } ?>
</div>

<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>

<div class="single-item-container">
  <div style="">
    <div class="header">
      <div><?= Html::img('@web/images/icons/'.$item->item_type.'.png', ['class'=>'icon']);?></div>
      <p class="title"><?= $item->title; ?></p>
      <p class="title"><?= $item->description; ?></p>
      <div class="image-container">
        <?= Html::img('@web/images/pricetag.png', ['class'=>'priceLabel']);?>
        <div class="priceText">$<?= $item->price; ?></div>
      </div>
    </div>
    <!--Todo use https://www.w3schools.com/howto/tryit.asp?filename=tryhow_css_custom_checkbox as a reference -->
  </div>
</div>

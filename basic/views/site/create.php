<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>


<div class="create-container">
  <div>
<?php $form = ActiveForm::begin(['options' => ['class' => 'create-form']]); ?>

    <?= $form->field($model, 'title')->textInput(['class' => 'customInput', 'spellcheck' => 'false', 'placeholder' => 'Title'])->label(false); ?>
    <?= $form->field($model, 'description')->textInput(['class' => 'customInput', 'spellcheck' => 'false', 'placeholder' => 'Description'])->label(false); ?>

    <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => 'button secondCustomButton']); ?>


<?php ActiveForm::end(); ?>
</div>
</div>

<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Types;

$a= ['fruit' => 'fruit',
'candy' => 'candy',
'beverages' => 'beverages',
'meat' => 'meat',
'dairy' => 'dairy',
'desert' => 'desert',
'vegetables' => 'vegetables',
'grain' => 'grain'];
?>



<div class="create-container">
  <div>
<?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['class' => 'customInput', 'spellcheck' => 'false', 'placeholder' => 'Title'])->label(false); ; ?>
    <?= $form->field($model, 'description')->textInput(['class' => 'customInput', 'spellcheck' => 'false', 'placeholder' => 'Description'])->label(false); ; ?>
    <?= $form->field($model, 'price')->textInput(['class' => 'customInput', 'spellcheck' => 'false', 'placeholder' => 'Price'])->label(false); ; ?>
    <?= $form->field($model, 'item_type')
    ->dropDownList($a, ['prompt'=>'Select Option']) ?>

    <?= $form->field($model, 'list_id')->hiddenInput(['value'=> $passedId])->label(false); ?>

    <?= Html::submitButton($model->isNewRecord ? 'Add' : 'Update', ['class' => 'button secondCustomButton']); ?>

<?php ActiveForm::end(); ?>
</div>
</div>

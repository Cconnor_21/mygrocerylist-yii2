<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
/* @var $this yii\web\View */

?>
<?= Html::a('Add grocery item', ['site/add', 'passedId' => $passedId], ['class' => 'button createButton']); ?>
<div class="grocery-items-container">

<?php foreach($items as $field){ ?>
    <div style="">
      <div class="header">
        <div><?= Html::img('@web/images/icons/'.$field->item_type.'.png', ['class'=>'icon']);?></div>
        <p class="title"><?= $field->title; ?></p>

        <div class="image-container">
          <?= Html::img('@web/images/pricetag.png', ['class'=>'priceLabel']);?>
          <div class="priceText">$<?= $field->price; ?></div>
        </div>
      </div>
      <?= Html::a("View", ['site/single', 'id' => $field->list_item_id], ['class' => 'button secondCustomButton']); ?>
      <!--Todo use https://www.w3schools.com/howto/tryit.asp?filename=tryhow_css_custom_checkbox as a reference -->
    </div>
<?php } ?>

</div>

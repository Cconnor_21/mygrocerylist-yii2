create database testdatabase
drop database testdatabase
use testdatabase

create database ajaxtest

create table comments(
	id int primary key auto_increment,
	comment varchar(200)
);

drop table List
drop table ListItem

create table ShoppingList(
	id int primary key AUTO_INCREMENT,
	title varchar(200),
	list_date datetime,
	description varchar(30)
);

create table ListItem(
	list_item_id int AUTO_INCREMENT,
	title varchar(200),
	description varchar(300),
	price decimal,
	item_type varchar(30),
	list_id int,
	PRIMARY KEY (list_item_id),
	FOREIGN KEY (list_id) references ShoppingList(id)
	
);

create table Types(
	id int primary key AUTO_INCREMENT,
	type varchar(30)
	
);

alter table ListItem
add foreign key (list_id) references ShoppingList(id);

select * from ShoppingList
select * from ListItem

insert into ShoppingList(title, description, list_date)
values('first shopping list', 'First description', '2008-11-11');
insert into ShoppingList(title, description, list_date)
values('second shopping list', 'Second description', '2008-11-11');
insert into ShoppingList(title, description, list_date)
values('fourth shopping list', 'Third description', '2008-11-11');

delete from ListItem where list_item_id = 1;

select * from ListItem

insert into ListItem(title, description, item_type, price, list_id)
values('Steak', 'this is a steak', 'meat', 5.00, 1);
insert into ListItem(title, description, item_type, price, list_id)
values('Apple', 'this is an apple', 'fruit', 2.00, 3);
insert into ListItem(title, description, item_type, price, list_id)
values('Diet Pepsi', 'this is the description for soda', 'beverages', 2.00, 2);
insert into ListItem(title, description, item_type, price, list_id)
values('chicken', 'this is the description for chicked', 'meat', 3.00, 1);

insert into Types(type)
values('fruit');
insert into Types(type)
values('candy');
insert into Types(type)
values('beverages');
insert into Types(type)
values('meat');
insert into Types(type)
values('dairy');
insert into Types(type)
values('desert');
insert into Types(type)
values('vegetables');
insert into Types(type)
values('grain');


select * from ListItem where id = 1;


